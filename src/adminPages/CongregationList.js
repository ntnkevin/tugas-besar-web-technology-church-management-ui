import React, { Component } from "react";
import UserApi from "../api/CongregationApi";
import Button from '../components/Button';
export default class Congregation extends Component {
  state = {
    congregations: null,
    info: null,
    selectedRecordId: null,
    fullname: "",
    address: "",
    birthdate: "",
    type: ""
  };

  routeChange(path) {
    this.props.history.push(path);
  }
  
  componentDidMount() {
    this.reloadCongregations();
  }

  async reloadCongregations() {
    const congregations = await UserApi.list();
    const info = await UserApi.info();
    this.setState({
      congregations,
      info
    });
  }
 
  async update() {
    const { fullname, address, birthdate, type, selectedRecordId } = this.state;
    const payload = {
      fullname,
      address,
      birthdate,
      type,
    };
    await UserApi.update(selectedRecordId, payload);
    this.reset();
    await this.reloadCongregations();
  }
  async delete(record) {
    await UserApi.delete(record.id);
    this.reset();
    await this.reloadCongregations();
  }
  
  reset() {
    this.setState({
      fullname: "",
      address: "",
      birthdate: "",
      type: "",
      selectedRecordId: null
    });
  }


  setValue(name, value) {
    this.setState({
      [name]: value
    });
  }

  get isReady() {
    const { congregations, info } = this.state;
    return congregations && info;
  }

  get isEditing() {
    return this.state.selectedRecordId && true;
  }

 

  edit(record) {
    this.setState({
      fullname: record.fullname || "",
      address: record.address || "",
      birthdate: record.birthdate || "",
      type: record.type || "",
      selectedRecordId: record.id,
      
    });
    this.routeChange(`/admin/congregation/${record.id}`);
  }
  
  render() {
    if (!this.isReady) return <div>Loading</div>;
    return (
      <div className="user">
        {this.renderTable()}
      </div>
    );
  }
  renderTable() {
    const recordElements = [];
    const { congregations, info } = this.state;
    for (const record of congregations) {
      recordElements.push(this.renderTableRecord(record));
    }

    return (
      <div className="user__section">
        <div className="user__section-content">
          <div className="user__section-header"><h2><center>Total Jemaat Gereja XXX: ({info.count})</center></h2></div>
          <table table id='congregations' class='table-congregation-list'>
            <thead>
              <tr class='table-congregation-list__th'>
                <th>Full Name</th>
                <th>Address</th>
                <th>Birth Date</th>
                <th>Type</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>{recordElements}</tbody>
          </table>
          <Button onClick={() => this.routeChange('/admin/congregation/create')}>Add Congregation</Button>
          <div className="user__section-header">Total Jemaat: ({info.count})</div>
        </div>
      </div>
    );
  }
  renderTableRecord(record) {
    const { fullname, address, birthdate, id } = record;
    return (
      <tr key={`row-${id}`}>
        <td>{fullname}</td>
        <td>{address}</td>
        <td>{birthdate}</td>
        <td>{record.CongregationType.type}</td>
        <td><Button onClick={() => this.edit(record)}>Edit</Button></td>
        <td><Button onClick={() => this.delete(record)}>Delete</Button></td>
      </tr>
    );
  }
}
