import React, { Component } from "react";
import {
  Button,
  Checkbox,
  RadioGroup,
  TextBox,
  MultiCheckbox,
  DropDown,
  DropDownContent,
  Select
} from "../components";

export default class Components extends Component {
  state = {
    isChecked: false,
    radioGroupOptions: [
      {
        text: "Male"
      },
      {
        text: "Female"
      },
      {
        text: "Other"
      }
    ],
    multiCheckboxOptions: [
      {
        text: "USB 3"
      },
      {
        text: "USB 3.1"
      },
      {
        text: "USB 3.1 gen 1"
      },
      {
        text: "USB 3.1 gen 2"
      }
    ],
    selectOptions: [
      {
        text: "Indonesian"
      },
      {
        text: "English"
      }
    ]
  };

  setValue(key, value) {
    this.setState({
      [key]: value
    });
  }

  onRadioGroupClick(option) {
    if (option === this.state.selectedRadioGroupOption) {
      this.setState({
        selectedRadioGroupOption: null
      });
    } else {
      this.setState({
        selectedRadioGroupOption: option
      });
    }
  }

  render() {
    return (
      <div className="components">
        {this.renderButton()}
        {this.renderCheckbox()}
        {this.renderRadioGroup()}
        {this.renderTextBox()}
        {this.renderMultiCheckbox()}
        {this.renderDropDown()}
        {this.renderSelect()}
      </div>
    );
  }

  renderValuePreview(name) {
    const value = this.state[name];
    let processedValue = value;
    if (typeof value === "object")
      processedValue = JSON.stringify(value, null, 4);
    return <pre>{processedValue}</pre>;
  }

  renderButton() {
    return (
      <div className="components__sample-container">
        <div className="components__sample-container-title">Button</div>
        <div className="components__sample-container-content">
          <div>
            <Button onClick={() => alert("Button pressed!")}>
              Theme Button
            </Button>
            <Button onClick={() => alert("Button pressed!")} type="success">
              Success Button
            </Button>
            <Button onClick={() => alert("Button pressed!")} type="danger">
              Danger Button
            </Button>
            <Button onClick={() => alert("Button pressed!")} type="link">
              Link Button
            </Button>
          </div>
          <div>
            <Button fullWidth>Full Width Button</Button>
          </div>
        </div>
      </div>
    );
  }

  renderCheckbox() {
    const { isChecked } = this.state;
    return (
      <div className="components__sample-container">
        <div className="components__sample-container-title">Checkbox</div>
        <div className="components__sample-container-content">
          <Checkbox
            isChecked={isChecked}
            onClick={value => this.setValue("isChecked", value)}
          >
            This is checkbox label
          </Checkbox>
        </div>
      </div>
    );
  }

  renderRadioGroup() {
    const { selectedRadioGroupOption, radioGroupOptions } = this.state;
    return (
      <div className="components__sample-container">
        <div className="components__sample-container-title">Radio Group</div>
        <div className="components__sample-container-content">
          <RadioGroup
            selectedOption={selectedRadioGroupOption}
            options={radioGroupOptions}
            onClick={option => this.onRadioGroupClick(option)}
          />
          {this.renderValuePreview("selectedRadioGroupOption")}
        </div>
      </div>
    );
  }

  renderTextBox() {
    const { textBoxValue } = this.state;
    return (
      <div className="components__sample-container">
        <div className="components__sample-container-title">TextBox</div>
        <div className="components__sample-container-content">
          <TextBox
            value={textBoxValue}
            label="Text Box Label"
            onChange={value => this.setValue("textBoxValue", value)}
          />
          {this.renderValuePreview("textBoxValue")}
        </div>
      </div>
    );
  }

  renderMultiCheckbox() {
    const { multiCheckboxOptions, selectedMultiCheckboxOptions } = this.state;
    return (
      <div className="components__sample-container">
        <div className="components__sample-container-title">Multi Checkbox</div>
        <div className="components__sample-container-content">
          <MultiCheckbox
            options={multiCheckboxOptions}
            selectedOptions={selectedMultiCheckboxOptions}
            onChange={value =>
              this.setValue("selectedMultiCheckboxOptions", value)
            }
          />
          {this.renderValuePreview("selectedMultiCheckboxOptions")}
        </div>
      </div>
    );
  }

  renderDropDown() {
    return (
      <div className="components__sample-container">
        <div className="components__sample-container-title">DropDown</div>
        <div className="components__sample-container-content">
          <DropDown>
            <Button>Click Here for activating DropDown</Button>
            <DropDownContent>
              <div className="components__sample-container">
                This is the content of dropdown
              </div>
            </DropDownContent>
          </DropDown>
        </div>
      </div>
    );
  }
  renderSelect() {
    const { selectOptions, selectSelectedOption } = this.state;
    return (
      <div className="components__sample-container">
        <div className="components__sample-container-title">Select</div>
        <div className="components__sample-container-content">
          <Select
            options={selectOptions}
            selectedOption={selectSelectedOption}
            onChange={value => this.setValue("selectSelectedOption", value)}
            label="Language"
          />
        </div>
      </div>
    );
  }
}
