import ApiClient from "../modules/ApiClient";

const client = new ApiClient({ baseUrl: "congregationservicerole" });

export default {
  get,
  list,
  insert,
  update,
  delete: del,
  info
};

async function get(id) {
  return client.get(id);
}

async function list(query) {
  return client.get("", query);
}

async function insert(data) {
  return client.post("", data);
}

async function update(id, data) {
  return client.put(id, data);
}

async function del(id) {
  return client.delete(id);
}

async function info(query) {
  return client.get("info");
}
