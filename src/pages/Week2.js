import React, { Component } from "react";
import RootApi from '../api/RootApi';

export default class Home2 extends Component {
  state = {
    info: '',
  };

  componentDidMount(){
      this.reloadInfo();
      this.sendMessage();
  }

  async reloadInfo(){
      const response = await RootApi.getInfo();
      this.setState({
          info: response.status,
      })
  }

  async sendMessage(){
      const response = await RootApi.send({message: 'this is my message'});
      this.setState({
        info: response.message,
      })
  }

  render() {
      const {info} = this.state;
    return (
      <div className="full-size">
        {info} <br />
        <button onClick={()=>this.sendMessage()}>Send Message</button>
      </div>
    );
  }
}
