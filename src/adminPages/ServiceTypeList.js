import React, { Component } from "react";
import ServicesTypeApi from "../api/ServiceTypeApi";
import Button from '../components/Button';
export default class ServiceType extends Component {
  state = {
    servicetypes: null,
    info: null,
    selectedRecordId: null,
    type: ""
  };

  routeChange(path) {
    this.props.history.push(path);
  }
  
  componentDidMount() {
    this.reloadServiceTypes();
  }

  async reloadServiceTypes() {
    const servicetypes = await ServicesTypeApi.list();
    const info = await ServicesTypeApi.info();
    this.setState({
      servicetypes,
      info
    });
  }
 
  async update() {
    const { type, selectedRecordId } = this.state;
    const payload = {
      type,
    };
    await ServicesTypeApi.update(selectedRecordId, payload);
    this.reset();
    await this.reloadServiceTypes();
  }
  async delete(record) {
    await ServicesTypeApi.delete(record.id);
    this.reset();
    await this.reloadServiceTypes();
  }
  
  reset() {
    this.setState({
      type: "",
      selectedRecordId: null
    });
  }


  setValue(name, value) {
    this.setState({
      [name]: value
    });
  }

  get isReady() {
    const { servicetypes, info } = this.state;
    return servicetypes && info;
  }

  get isEditing() {
    return this.state.selectedRecordId && true;
  }

 

  edit(record) {
    this.setState({
      type: record.type || "",
      selectedRecordId: record.id,
      
    });
    this.routeChange(`/admin/servicetype/${record.id}`);
  }
  
  render() {
    if (!this.isReady) return <div>Loading</div>;
    return (
      <div className="user">
        {this.renderTable()}
      </div>
    );
  }
  renderTable() {
    const recordElements = [];
    const { info, servicetypes } = this.state;
    for (const record of servicetypes) {
      recordElements.push(this.renderTableRecord(record));
    }

    return (
      <div className="user__section">
        <div className="user__section-content">
          <div className="user__section-header"><h2><center>Tipe Kebaktian Gereja XXX: ({info.count})</center></h2></div>
          <table table id='servicetypes' class='table-congregation-list'>
            <thead>
              <tr class='table-congregation-list__th'>
                <th>Type</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>{recordElements}</tbody>
          </table>
          <Button onClick={() => this.routeChange('/admin/servicetype/create')}>Add Service</Button>
        </div>
      </div>
    );
  }
  renderTableRecord(record) {
    const { id,type } = record;
    return (
      <tr key={`row-${id}`}>
        <td>{type}</td>
        <td><Button onClick={() => this.edit(record)}>Edit</Button></td>
        <td><Button onClick={() => this.delete(record)}>Delete</Button></td>
      </tr>
    );
  }
}
