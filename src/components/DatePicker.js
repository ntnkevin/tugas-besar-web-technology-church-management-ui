import React ,  { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class DatePickerC extends Component {
    state = {
        startDate: new Date()
      };

      handleChange = dateandtime => {
        this.setState({
            date: dateandtime
        });
    }

    handleSelect = selected => {
        this.setState({
            dateandtime: selected
        })
    }
      render() {
        return (
          <DatePicker
            selected={this.state.date}
            onChange={this.handleChange}
          />
        );
      }
    }