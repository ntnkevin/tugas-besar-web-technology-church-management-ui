import React, { Component } from "react";
import RootApi from '../api/RootApi';
import Button from'../components/Button';
import TextBox from'../components/TextBox';

export default class Home2 extends Component {
  state = {
    email: '',
    password: '',
  };

  componentDidMount(){
      this.reloadInfo();
      this.sendMessage();
  }

  async reloadInfo(){
      const response = await RootApi.getInfo();
      this.setState({
          info: response.status,
      })
  }

  async sendMessage(){
      const response = await RootApi.send({message: 'this is my message'});
      this.setState({
        info: response.message,
      })
  }

  render() {
    const { email,password} = this.state;
    return (
      <div className="full-size">
        <TextBox 
         label="E-mail"
         value={email}
         onChange={(value)=> this.setState({email:value})}
         ></TextBox>
        <TextBox 
          label="password"
          password={password}
          onChange={(value) => this.setState({password:value})}
          ></TextBox>
          <Button>Login</Button>
      </div>
    );
  }
}
