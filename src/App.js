import React, { Component } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import "./App.scss";
import { MainLayout, AdminLayout } from "./layouts";
import appConfig from "./config/app";

class App extends Component {
  state = {};

  componentDidMount() {
    console.log(appConfig);
  }

  isReady() {
    return true;
  }

  getExtendedProps(props) {
    return {
      ...props,
      navigate: (target, state) => props.history.push(target, state)
    };
  }

  render() {
    const isReady = this.isReady();
    if (!isReady) {
      return null;
    }
    return <HashRouter>{this.renderLayoutRoutes()}</HashRouter>;
  }

  renderLayoutRoutes() {
    return (
      <Switch>
        <Route
          path="/admin"
          component={props => <AdminLayout {...this.getExtendedProps(props)} />}
        />
        <Route
          path="/"
          component={props => <MainLayout {...this.getExtendedProps(props)} />}
        />
      </Switch>
    );
  }
}

export default App;
