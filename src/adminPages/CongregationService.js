import React, { Component } from "react";
import CongregationServiceApi from "../api/CongregationServicesApi";

export default class User extends Component {
  state = {
    users: null,
    info: null,
    selectedRecordId: null,
    modeTitle: "New Record",

    id: "",
    congregationid: "",
    serviceid: "",
    role: "",
  };

  componentDidMount() {
    this.reloadCongregationsService();
  }

  async reloadCongregationsService() {
    const congregationsServices = await CongregationServiceApi.list();
    const info = await CongregationServiceApi.info();
    this.setState({
      congregationsServices,
      info
    });
  }

  setValue(name, value) {
    this.setState({
      [name]: value
    });
  }

  get isReady() {
    const { congregationsServices, info } = this.state;
    return congregationsServices && info;
  }

  get isEditing() {
    return this.state.selectedRecordId && true;
  }

  get canSubmit() {
    const { congregationid, serviceid, role, selectedRecordId } = this.state;
    if (!selectedRecordId && congregationid.length === 0) {
      return false;
    }
    return congregationid.length !== 0 && serviceid.length !== 0 && role.length !== 0;
  }

  async insert() {
    const { id, congregationid, serviceid, role,  } = this.state;
    const payload = {
      id,
      congregationid,
      serviceid,
      role,
      
    };
    await CongregationServiceApi.insert(payload);
    this.reset();
    await this.reloadCongregationsService();
  }

  async update() {
    const { congregationid, serviceid, role, selectedRecordId } = this.state;
    const payload = {
      congregationid,
      serviceid,
      role,
      
    };
    await CongregationServiceApi.update(selectedRecordId, payload);
    this.reset();
    await this.reloadCongregationsService();
  }

  async delete(record) {
    await CongregationServiceApi.delete(record.id);
    this.reset();
    await this.reloadCongregationsService();
  }

  edit(record) {
    this.setState({
      congregationid: record.congregationid || "",
      serviceid: record.serviceid || "",
      role: record.role || "",
      selectedRecordId: record.id
    });
  }

  reset() {
    this.setState({
      congregationid: "",
      serviceid: "",
      role: "",
      selectedRecordId: null
    });
  }

  render() {
    if (!this.isReady) return <div>Loading</div>;
    return (
      <div className="user">
        {this.renderForm()}
        {this.renderTable()}
      </div>
    );
  }

  renderForm() {
    const { modeTitle, congregationid, serviceid, role } = this.state;
    return (
      <div className="user__section">
        <div className="user__section-header">{modeTitle}</div>
        <div className="user__section-content">
          <table>
            <tr>
              <th>congregationid</th>
              <td>
                <input
                  type="text"
                  value={congregationid}
                  onChange={event => this.setValue("congregationid", event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>serviceid</th>
              <td>
                <input
                  type="text"
                  value={serviceid}
                  onChange={event => this.setValue("serviceid", event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>role</th>
              <td>
                <input
                  type="text"
                  value={role}
                  onChange={event => this.setValue("role", event.target.value)}
                />
              </td>
            </tr>
          </table>
          {this.renderFormButtons()}
        </div>
      </div>
    );
  }

  renderFormButtons() {
    if (!this.isEditing)
      return (
        <>
          <button disabled={!this.canSubmit} onClick={() => this.insert()}>
            Submit
          </button>
          <button onClick={() => this.reset()}>Reset</button>
        </>
      );
    return (
      <>
        <button disabled={!this.canSubmit} onClick={() => this.update()}>
          Save
        </button>
        <button onClick={() => this.reset()}>Cancel</button>
      </>
    );
  }

  renderTable() {
    const recordElements = [];
    const { congregationsServices, info } = this.state;
    for (const record of congregationsServices) {
      recordElements.push(this.renderTableRecord(record));
    }
    return (
      <div className="user__section">
        <div className="user__section-header">Records ({info.count})</div>
        <div className="user__section-content">
          <table className="user__table">
            <thead>
              <tr>
                <th>Id</th>
                <th>Congregationid</th>
                <th>Serviceid</th>
                <th>Role</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>{recordElements}</tbody>
          </table>
        </div>
      </div>
    );
  }

  renderTableRecord(record) {
    const { congregationid, serviceid, role, id } = record;
    return (
      <tr>
        <td>{id}</td>
        <td>{congregationid}</td>
        <td>{serviceid}</td>
        <td>{role}</td>
        <td>
          <button onClick={() => this.edit(record)}>Edit</button>
          <button onClick={() => this.delete(record)}>Delete</button>
        </td>
      </tr>
    );
  }
}
