import React, { Component } from "react";
import UserApi from "../api/CongregationApi";
import DatePicker from '../components/DatePicker';
import DropDown from "../components/DropDown";
export default class Dashboard extends Component {
  state = {
    congregations: null,
    info: null,
    selectedRecordId: null,
    fullname: "",
    address: "",
    birthdate: new Date(),
    type: ""
  };

  routeChange(path) {
    this.props.history.push(path);
  }

  reset() {
    this.setState({
      fullname: "",
      address: "",
      birthdate: "",
      type: "",
      selectedRecordId: null
    });
  }

  get canSubmit() {
    const { fullname, address, birthdate, type, selectedRecordId } = this.state;
    if (!selectedRecordId === 0) {
      return false;
    }
    return fullname.length !== 0 && address.length !== 0 && birthdate.length !== 0 && type.length !== 0;
  }
  async insert() {
    const { fullname, address, birthdate, type } = this.state;
    const payload = {
      fullname,
      address,
      birthdate,
      type
    };

    await UserApi.insert(payload);
    this.routeChange('/admin/congregation-list');
    this.reset();
    await this.reloadCongregations();
  }

  setValue(name, value) {
    this.setState({
      [name]: value
    });
  }
  render() {
    const { fullname, address, birthdate, type } = this.state;
    return (
      <div className="full-size flex-center">
        <form class='form-add-congregation' id='form-add'>
          <center><h3>Add Congregation</h3></center>
          <br />
          <table>
            <tr>
              <td>
                <label>
                  <b>
                    Full Name:
              </b>
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <label>
                  <input
                    name="fullName"
                    type="text"
                    size="48"
                    placeholder="Full Name"
                    value={fullname}
                    onChange={event =>
                      this.setValue("fullname", event.target.value)
                    } />
                </label>
              </td>
            </tr>

            <br />

            <tr>
              <td>
                <label>
                  <b>
                    Address:
            </b>
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <label>
                  <input
                    name="address"
                    type="text"
                    size="48"
                    placeholder="Address"
                    value={address}
                    onChange={event =>
                      this.setValue("address", event.target.value)
                    } />
                </label>
              </td>
            </tr>

            <br />

            <tr>
              <td>
                <label>
                  <b>
                    Birth Date:
            </b>
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <label>
                <DatePicker
                                    selected={this.state.birthdate}
                                    onSelect={this.handleSelect} //when day is clicked
                                    onChange={this.handleChange} //only when value has changed
                                />
                </label>
              </td>
            </tr>

            <br />

            <tr>
              <td>
                <label>
                  <b>
                    Congregation Type:
            </b>
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <label>
                
                  
                <select 
                id="congregation-type"
                placeholder="Congregation Type"
                type="text"
                // value={type}
                onChange={event =>
                  this.setValue("type", event.target.value)
                }
                >
                <option value="Jemaat">Jemaat</option>
                <option value="Anggota">Anggota</option>
                <option value="Sukarelawan">Sukarelawan</option>
                </select>

{/*        
                  <input
                    name="congregationType"
                    type="text"
                    size="48"
                    placeholder="Congregation Type"
                    value={type}
                     />  */}
                </label>
              </td>
            </tr>
          </table>

          <br />

          <label><button class="button-submit-add-congregation" disabled={!this.canSubmit} onClick={() => this.insert()}><b>Submit</b></button></label>
          <label><button class="button-reset-add-congregation" onClick={() => this.reset()}><b>Reset</b></button></label>

        </form>
      </div>
    );
  }
}

// function show_selected() {
//   var selector = document.getElementById('id_of_select');
//   var value = selector[selector.selectedIndex].value;

//   document.getElementById('congregationType').innerHTML = value;
// }
// // document.getElementById('btn').addEventListener('click', show_selected);;