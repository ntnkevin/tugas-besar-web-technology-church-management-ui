import React, { Component } from "react";

export default class Home extends Component {
  state = {};

  render() {
    return (
      <div className="full-size flex-center">Welcome to the home page</div>
    );
  }
}
