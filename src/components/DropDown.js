import React, { Component } from "react";
import DropDownContent from "./DropDownContent";

export default class DropDown extends Component {
  static defaultProps = {
    className: ""
  };

  state = {
    isVisible: false
  };

  show() {
    this.setState({
      isVisible: true
    });
  }

  hide() {
    this.setState({
      isVisible: false
    });
  }

  toggleDropDown() {
    const { isVisible } = this.state;
    this.setState({
      isVisible: !isVisible
    });
  }

  getDropdownChildren() {
    const { children } = this.props;
    const { isVisible } = this.state;
    const result = [];
    for (const child of children) {
      if (typeof child !== "object") {
        result.push(child);
      } else {
        const isDropdownContent =
          child.type.prototype === DropDownContent.prototype;
        if (!isDropdownContent || isVisible) {
          result.push(child);
        }
      }
    }
    return result;
  }

  render() {
    const { className } = this.props;
    return (
      <div
        className={`dropdown ${className}`}
        onClick={() => this.toggleDropDown()}
      >
        {this.getDropdownChildren()}
      </div>
    );
  }
}
