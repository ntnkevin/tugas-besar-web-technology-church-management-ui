import React, { Component } from "react";
import { isEmpty } from "../utils";
import uuid from "uuid/v4";

export default class TextBox extends Component {
  static defaultProps = {
    label: "",
    value: "",
    type: "text",
    className: "",
    onChange: () => {}
  };
  id = uuid();

  state = {
    isFocused: false
  };

  getStateClass() {
    const { isFocused } = this.state;
    const { value } = this.props;
    if (isFocused || !isEmpty(value)) {
      return "textbox--active";
    }
    return "";
  }

  onFocus() {
    this.setState({
      isFocused: true
    });
  }

  onBlur() {
    this.setState({
      isFocused: false
    });
  }

  onChange(event) {
    this.props.onChange(event.target.value);
  }

  render() {
    const { className } = this.props;
    const stateClass = this.getStateClass();
    return (
      <div className={`textbox ${stateClass} ${className}`}>
        {this.renderLabel()}
        {this.renderInput()}
      </div>
    );
  }

  renderLabel() {
    const { label } = this.props;
    return (
      <label className="textbox__label" for={this.id}>
        {label}
      </label>
    );
  }

  renderInput() {
    const { value, type } = this.props;
    return (
      <input
        className="textbox__input"
        value={value}
        type={type}
        onFocus={() => this.onFocus()}
        onBlur={() => this.onBlur()}
        onChange={event => this.onChange(event)}
        id={this.id}
      />
    );
  }
}
