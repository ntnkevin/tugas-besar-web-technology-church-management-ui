import React, { Component } from "react";
import ServicesApi from "../api/ServiceApi";
export default class UpdateService extends Component {
    state = {
        services: null,
        // info: null,
        selectedRecordId: null,
        title: "",
        dateandtime: "",
        type: ""
    };

    constructor(props) {
        super(props)
        this.state.id = this.props.match.params.id;
    }

    routeChange(path) {
        this.props.history.push(path);
    }

    async update() {
        const { title, dateandtime, type, id } = this.state;
        const payload = {
            title,
            dateandtime,
            type,
        };

        await ServicesApi.update(id, payload);
        this.routeChange('/admin/service');
        await this.reloadCongregations();
    }

    componentDidMount() {
        this.reloadUser();
    }

    async reloadUser() {
        const { id } = this.state;
        const user = await ServicesApi.get(id);
        const { title, dateandtime, type } = user;
        this.setState({
            title,
            dateandtime,
            type
        });
    }

    reset() {
        this.setState({
            fullname: "",
            address: "",
            birthdate: "",
            type: "",
        });
    }


    setValue(name, value) {
        this.setState({
            [name]: value
        });
    }
    render() {
        const { title, dateandtime, type } = this.state;
        return (
            <div className="full-size flex-center">
                <form class='form-add-congregation' id='form-add'>
                    <center><h3>Update Service</h3></center>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <label><b>Title:</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input
                                        name="title"
                                        type="text"
                                        size="48"
                                        placeholder="Title"
                                        value={title}
                                        onChange={event =>
                                            this.setValue("title", event.target.value)
                                        } />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><b>Date And Time:</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input
                                        name="dateandtime"
                                        type="text"
                                        size="48"
                                        placeholder="Date And Time"
                                        value={dateandtime}
                                        onChange={event =>
                                            this.setValue("dateandtime", event.target.value)
                                        } />
                                </label>
                            </td>
                        </tr>

                        <br />

                        <tr>
                            <td>
                                <label>
                                    <b>
                                        Service Type:
            </b>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input
                                        name="servicetype"
                                        type="text"
                                        size="48"
                                        placeholder="Service Type"
                                        value={type}
                                        onChange={event =>
                                            this.setValue("type", event.target.value)
                                        } />
                                </label>
                            </td>
                        </tr>
                    </table>

                    <br />

                    <label><button class="button-submit-add-congregation" onClick={() => this.update()}><b>Update</b></button></label>
                    <label><button class="button-reset-add-congregation" onClick={() => this.reset()}><b>Reset</b></button></label>

                </form>
            </div>
        );
    }
}