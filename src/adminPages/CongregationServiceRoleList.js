import React, { Component } from "react";
import CongregationServiceRoleApi from "../api/CongregationServiceRoleApi";
import Button from '../components/Button';
export default class CongregationServiceRoleList extends Component {
  state = {
    congregationserviceroles: null,
    info: null,
    selectedRecordId: null,
    role: ""
  };

  routeChange(path) {
    this.props.history.push(path);
  }
  
  componentDidMount() {
    this.reloadCongregationServiceRoles();
  }

  async reloadCongregationServiceRoles() {
    const congregationserviceroles = await CongregationServiceRoleApi.list();
    const info = await CongregationServiceRoleApi.info();
    this.setState({
      congregationserviceroles,
      info
    });
  }
 
  async update() {
    const { role, selectedRecordId } = this.state;
    const payload = {
      role,
    };
    await CongregationServiceRoleApi.update(selectedRecordId, payload);
    this.reset();
    await this.reloadCongregationServiceRoles();
  }
  async delete(record) {
    await CongregationServiceRoleApi.delete(record.id);
    this.reset();
    await this.reloadCongregationServiceRoles();
  }
  
  reset() {
    this.setState({
      role: "",
      selectedRecordId: null
    });
  }


  setValue(name, value) {
    this.setState({
      [name]: value
    });
  }

  get isReady() {
    const { congregationserviceroles, info } = this.state;
    return congregationserviceroles && info;
  }

  get isEditing() {
    return this.state.selectedRecordId && true;
  }

 

  edit(record) {
    this.setState({
      role: record.role || "",
      selectedRecordId: record.id,
      
    });
    this.routeChange(`/admin/congregationservicerole/${record.id}`);
  }
  
  render() {
    if (!this.isReady) return <div>Loading</div>;
    return (
      <div className="user">
        {this.renderTable()}
      </div>
    );
  }
  renderTable() {
    const recordElements = [];
    const { info, congregationserviceroles } = this.state;
    for (const record of congregationserviceroles) {
      recordElements.push(this.renderTableRecord(record));
    }

    return (
      <div className="user__section">
        <div className="user__section-content">
          <div className="user__section-header"><h2><center>Role Pelayan Gereja XXX: ({info.count})</center></h2></div>
          <table table id='congregationserviceroles' class='table-congregation-list'>
            <thead>
              <tr class='table-congregation-list__th'>
                <th>role</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>{recordElements}</tbody>
          </table>
          <Button onClick={() => this.routeChange('/admin/congregationservicerole/create')}>Add Service</Button>
        </div>
      </div>
    );
  }
  renderTableRecord(record) {
    const { id,role } = record;
    return (
      <tr key={`row-${id}`}>
        <td>{role}</td>
        <td><Button onClick={() => this.edit(record)}>Edit</Button></td>
        <td><Button onClick={() => this.delete(record)}>Delete</Button></td>
      </tr>
    );
  }
}
