import React, { Component } from "react";
import CongregationsServiceRoleApi from "../api/CongregationServiceRoleApi";
export default class UpdateCongregationType extends Component {
    state = {
        services: null,
        // info: null,
        selectedRecordId: null,
        role: ""
    };

    constructor(props) {
        super(props)
        this.state.id = this.props.match.params.id;
    }

    routeChange(path) {
        this.props.history.push(path);
    }

    async update() {
        const { role, id } = this.state;
        const payload = {
            role,
        };

        await CongregationsServiceRoleApi.update(id, payload);
        this.routeChange('/admin/congregationservicerole');
        await this.reloadCongregationType();
    }

    componentDidMount() {
        this.reloadUser();
    }

    async reloadUser() {
        const { id } = this.state;
        const user = await CongregationsServiceRoleApi.get(id);
        const { role } = user;
        this.setState({
            role
        });
    }

    reset() {
        this.setState({
            role: "",
        });
    }


    setValue(name, value) {
        this.setState({
            [name]: value
        });
    }
    render() {
        const { title, role } = this.state;
        return (
            <div className="full-size flex-center">
                <form class='form-add-congregation' id='form-add'>
                    <center><h3>Update CongregationService Role</h3></center>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <label><b>Role:</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input
                                        name="role"
                                        role="text"
                                        size="48"
                                        placeholder="Role"
                                        value={title}
                                        onChange={event =>
                                            this.setValue("role", event.target.value)
                                        } />
                                </label>
                            </td>
                        </tr>
                    </table>

                    <br />

                    <label><button class="button-submit-add-congregation" onClick={() => this.update()}><b>Update</b></button></label>
                    <label><button class="button-reset-add-congregation" onClick={() => this.reset()}><b>Reset</b></button></label>

                </form>
            </div>
        );
    }
}