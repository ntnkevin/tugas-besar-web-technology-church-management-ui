import React, { Component } from "react";
import ServicesTypeApi from "../api/ServiceTypeApi";
export default class UpdateServiceType extends Component {
    state = {
        services: null,
        // info: null,
        selectedRecordId: null,
        type: ""
    };

    constructor(props) {
        super(props)
        this.state.id = this.props.match.params.id;
    }

    routeChange(path) {
        this.props.history.push(path);
    }

    async update() {
        const { type, id } = this.state;
        const payload = {
            type,
        };

        await ServicesTypeApi.update(id, payload);
        this.routeChange('/admin/servicetype');
        await this.reloadServiceType();
    }

    componentDidMount() {
        this.reloadServiceType();
    }

    async reloadServiceType() {
        const { id } = this.state;
        const user = await ServicesTypeApi.get(id);
        const { type } = user;
        this.setState({
            type
        });
    }

    reset() {
        this.setState({
            type: "",
        });
    }


    setValue(name, value) {
        this.setState({
            [name]: value
        });
    }
    render() {
        const { title, dateandtime, type } = this.state;
        return (
            <div className="full-size flex-center">
                <form class='form-add-congregation' id='form-add'>
                    <center><h3>Update Service</h3></center>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <label><b>Type:</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input
                                        name="type"
                                        type="text"
                                        size="48"
                                        placeholder="Type"
                                        value={title}
                                        onChange={event =>
                                            this.setValue("type", event.target.value)
                                        } />
                                </label>
                            </td>
                        </tr>
                    </table>

                    <br />

                    <label><button class="button-submit-add-congregation" onClick={() => this.update()}><b>Update</b></button></label>
                    <label><button class="button-reset-add-congregation" onClick={() => this.reset()}><b>Reset</b></button></label>

                </form>
            </div>
        );
    }
}