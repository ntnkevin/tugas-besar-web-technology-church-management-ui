import React, { Component } from "react";
import DropDown from "./DropDown";
import DropDownContent from "./DropDownContent";
import { isEmpty } from "../utils";
import Icon from "./Icon";

export default class Select extends Component {
  static defaultProps = {
    onChange: () => {},
    label: "",
    className: "",
    selectedOption: null,
    options: [], // {text: string}
    textKey: "text"
  };

  state = {};

  state = {};

  getStateClass() {
    const { selectedOption } = this.props;
    if (!isEmpty(selectedOption)) {
      return "select--active";
    }
    return "";
  }

  getOptionText(option) {
    const { textKey } = this.props;
    if (!option) {
      return "";
    }
    return option[textKey];
  }

  onChange(option) {
    this.props.onChange(option);
  }

  render() {
    const { className } = this.props;
    const stateClass = this.getStateClass();
    return (
      <DropDown className={`select ${stateClass} ${className}`}>
        {this.renderLabel()}
        {this.renderInputView()}

        <span className="select__caret-container">
          <Icon name="caretDown" />
        </span>
        <DropDownContent className="select__options-container">
          {this.renderOptions()}
        </DropDownContent>
      </DropDown>
    );
  }

  renderLabel() {
    const { label } = this.props;
    return <div className="select__label">{label}</div>;
  }

  renderInputView() {
    const { selectedOption } = this.props;
    return (
      <div className="select__input">{this.getOptionText(selectedOption)}</div>
    );
  }

  renderOptions() {
    const { options } = this.props;
    const optionElements = [];
    for (const option of options) {
      optionElements.push(this.renderOption(option));
    }
    return optionElements;
  }

  renderOption(option) {
    return (
      <div
        key={`select-option-${option.text}`}
        className="select__option"
        onClick={() => this.onChange(option)}
      >
        {this.getOptionText(option)}
      </div>
    );
  }
}
