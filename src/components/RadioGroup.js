import React, { Component } from "react";
import Checkbox from "./Checkbox";

export default class RadioGroup extends Component {
  static defaultProps = {
    options: [], // {text: string}
    selectedOption: undefined,
    onClick: () => {}
  };

  state = {};

  onClick(option) {
    this.props.onClick(option);
  }

  render() {
    return <div className="radio-group">{this.renderOptions()}</div>;
  }

  renderOptions() {
    const { options } = this.props;
    const optionElements = [];
    for (const option of options) {
      optionElements.push(this.renderOption(option));
    }
    return optionElements;
  }

  renderOption(option) {
    const { selectedOption } = this.props;
    const { text } = option;
    const isChecked = selectedOption === option;
    return (
      <div className="radio-group__option" key={`radio-option-${text}`}>
        <Checkbox isChecked={isChecked} onClick={() => this.onClick(option)}>
          {text}
        </Checkbox>
      </div>
    );
  }
}
