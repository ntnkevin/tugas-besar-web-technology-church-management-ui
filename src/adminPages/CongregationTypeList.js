import React, { Component } from "react";
import CongregationTypeApi from "../api/CongregationTypeApi";
import Button from '../components/Button';
export default class CongregationType extends Component {
  state = {
    congregationtypes: null,
    info: null,
    selectedRecordId: null,
    type: ""
  };

  routeChange(path) {
    this.props.history.push(path);
  }
  
  componentDidMount() {
    this.reloadCongregationTypes();
  }

  async reloadCongregationTypes() {
    const congregationtypes = await CongregationTypeApi.list();
    const info = await CongregationTypeApi.info();
    this.setState({
      congregationtypes,
      info
    });
  }
 
  async update() {
    const { type, selectedRecordId } = this.state;
    const payload = {
      type,
    };
    await CongregationTypeApi.update(selectedRecordId, payload);
    this.reset();
    await this.reloadCongregationTypes();
  }
  async delete(record) {
    await CongregationTypeApi.delete(record.id);
    this.reset();
    await this.reloadCongregationTypes();
  }
  
  reset() {
    this.setState({
      type: "",
      selectedRecordId: null
    });
  }


  setValue(name, value) {
    this.setState({
      [name]: value
    });
  }

  get isReady() {
    const { congregationtypes, info } = this.state;
    return congregationtypes && info;
  }

  get isEditing() {
    return this.state.selectedRecordId && true;
  }

 

  edit(record) {
    this.setState({
      type: record.type || "",
      selectedRecordId: record.id,
      
    });
    this.routeChange(`/admin/congregationtype/${record.id}`);
  }
  
  render() {
    if (!this.isReady) return <div>Loading</div>;
    return (
      <div className="user">
        {this.renderTable()}
      </div>
    );
  }
  renderTable() {
    const recordElements = [];
    const { congregationtypes, info } = this.state;
    for (const record of congregationtypes) {
      recordElements.push(this.renderTableRecord(record));
    }

    return (
      <div className="user__section">
        <div className="user__section-content">
          <div className="user__section-header"><h2><center>Tipe Jemaat Gereja XXX: ({info.count})</center></h2></div>
          <table table id='congregationtypes' class='table-congregation-list'>
            <thead>
              <tr class='table-congregation-list__th'>
                <th>Type</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>{recordElements}</tbody>
          </table>
          <Button onClick={() => this.routeChange('/admin/congregationtype/create')}>Add Service</Button>
        </div>
      </div>
    );
  }
  renderTableRecord(record) {
    const { id,type } = record;
    return (
      <tr key={`row-${id}`}>
        <td>{type}</td>
        <td><Button onClick={() => this.edit(record)}>Edit</Button></td>
        <td><Button onClick={() => this.delete(record)}>Delete</Button></td>
      </tr>
    );
  }
}
