import React, { Component } from "react";
import ServicesApi from "../api/ServiceApi";
import Button from '../components/Button';
export default class Service extends Component {
  state = {
    services: null,
    info: null,
    selectedRecordId: null,
    title: "",
    dateandtime: "",
    type: ""
  };

  routeChange(path) {
    this.props.history.push(path);
  }
  
  componentDidMount() {
    this.reloadServices();
  }

  async reloadServices() {
    const services = await ServicesApi.list();
    const info = await ServicesApi.info();
    this.setState({
      services,
      info
    });
  }
 
  async update() {
    const { title, dateandtime, type, selectedRecordId } = this.state;
    const payload = {
      title,
      dateandtime,
      type,
    };
    await ServicesApi.update(selectedRecordId, payload);
    this.reset();
    await this.reloadServices();
  }
  
  async delete(record) {
    await ServicesApi.delete(record.id);
    this.reset();
    await this.reloadServices();
  }
  
  reset() {
    this.setState({
      title: "",
      dateandtime: "",
      type: "",
      selectedRecordId: null
    });
  }


  setValue(name, value) {
    this.setState({
      [name]: value
    });
  }

  get isReady() {
    const { services, info } = this.state;
    return services && info;
  }

  get isEditing() {
    return this.state.selectedRecordId && true;
  }

 

  edit(record) {
    this.setState({
      title: record.title || "",
      dateandtime: record.dateandtime || "",
      type: record.type || "",
      selectedRecordId: record.id,
      
    });
    this.routeChange(`/admin/service/${record.id}`);
  }
  
  render() {
    if (!this.isReady) return <div>Loading</div>;
    return (
      <div className="user">
        {this.renderTable()}
      </div>
    );
  }
  renderTable() {
    const recordElements = [];
    const { info, services } = this.state;
    for (const record of services) {
      recordElements.push(this.renderTableRecord(record));
    }

    return (
      <div className="user__section">
        <div className="user__section-content">
          <div className="user__section-header"><h2><center>Kebaktian Gereja XXX: ({info.count})</center></h2></div>
          <table table id='services' class='table-congregation-list'>
            <thead>
              <tr class='table-congregation-list__th'>
                <th>Title</th>
                <th>Date And Time</th>
                <th>Type</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>{recordElements}</tbody>
          </table>
          <Button onClick={() => this.routeChange('/admin/service/create')}>Add Service</Button>
        </div>
      </div>
    );
  }
  renderTableRecord(record) {
    const { title, dateandtime, type, id } = record;
    return (
      <tr key={`row-${id}`}>
        <td>{title}</td>
        <td>{dateandtime}</td>
        <td>{record.ServiceType.type}</td>
        <td><Button onClick={() => this.edit(record)}>Edit</Button></td>
        <td><Button onClick={() => this.delete(record)}>Delete</Button></td>
      </tr>
    );
  }
}
