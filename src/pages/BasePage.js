import { Component } from "react";

export default class BasePage extends Component {
  setValue(field,event){
    this.setState({
      [field]: event.target.value,
    });
  }

}
