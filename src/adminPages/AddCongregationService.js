import React, { Component } from "react";
import CongregationServiceApi from "../api/CongregationServiceApi";
import DatePicker from '../components/DatePicker';
export default class Dashboard extends Component {
    state = {
        services: null,
        info: null,
        selectedRecordId: null,
        //modeTitle: "New Record",
        serviceid: "",
        congregationid: "",
        role: "",
    };

    routeChange(path) {
        this.props.history.push(path);
    }

    reset() {
        this.setState({
            serviceid: "",
            congregationid: "",
            role: "",
            selectedRecordId: null
        });
    }

    get canSubmit() {
        const { serviceid, congregationid, role, selectedRecordId } = this.state;
        if (!selectedRecordId === 0) {
            return false;
        }
        return serviceid.length !== 0 && congregationid.length !== 0 && role.length !== 0;
    }
    async insert() {
        const { serviceid, congregationid, role } = this.state;
        const payload = {
            serviceid,
            congregationid, 
            role
        };

        await CongregationServiceApi.insert(payload);
        this.routeChange('/admin/congregationservice');
        this.reset();
        await this.reloadCongregationService();
    }

    setValue(name, value) {
        this.setState({
            [name]: value
        });
    }
    render() {
        const { title, type } = this.state;
        return (
            <div className="full-size flex-center">
                <form class='form-add-congregation' id='form-add'>
                    <center><h3>Add CongregationService</h3></center><br />
                    <table><tr>
                        <td>
                            <label><b>Service:</b></label>
                        </td>
                    </tr>
                        <tr>
                            <td>
                                <label>
                                    <input
                                        name="serviceid"
                                        type="text"
                                        size="48"
                                        placeholder="Title of Service"
                                        value={title}
                                        onChange={event => this.setValue("serviceid", event.target.value)}
                                    />
                                </label>
                            </td>
                        </tr>
                        <br />
                        <tr><td><label><b>Congregation:</b>
                        </label>
                        </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input
                                        name="congregationid"
                                        type="text"
                                        size="48"
                                        placeholder="Title of Congregation"
                                        value={title}
                                        onChange={event => this.setValue("congregationid", event.target.value)}
                                    />
                                </label>
                            </td>
                        </tr>
                        <br />
                        <tr>
                            <td><label><b>Role:</b></label></td>
                        </tr>
                        <tr>
                            <td>
                                <input
                                    name="role"
                                    type="text"
                                    size="48"
                                    placeholder="Service Type"
                                    value={type}
                                    onChange={event =>
                                        this.setValue("role", event.target.value)} 
                                />
                            </td>
                        </tr>
                    </table>

                    <br />

                    <label><button class="button-submit-add-congregation" disabled={!this.canSubmit} onClick={() => this.insert()}><b>Submit</b></button></label>
                    <label><button class="button-reset-add-congregation" onClick={() => this.reset()}><b>Reset</b></button></label>

                </form>
            </div>
        );
    }
}