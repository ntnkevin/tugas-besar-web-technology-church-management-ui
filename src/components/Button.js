import React, { Component } from 'react';

export default class Button extends Component {
  static defaultProps = {
  type: 'theme',
  onClick: () => {},
  disabled: false,
  fullWidth: false,
  className: '',
};

  state = {};


  getClassName() {
  const { type, fullWidth, margin } = this.props;
  return `button--${type} ${fullWidth ? 'button--full-width' : ''} ${
    margin ? 'button--with-margin' : ''
  }`;
}

  render() {
  const { children, onClick, disabled, className } = this.props;
  return (
    <button
      disabled={disabled}
      className={` button ${this.getClassName()} ${className}`}
      onClick={() => onClick()}
      type='button'
    >
      {children}
    </button>
  );
}
}
