import React, { Component } from "react";
import UserApi from "../api/UserApi";

export default class User extends Component {
  state = {
    users: null,
    info: null,
    selectedRecordId: null,
    modeTitle: "New Record",

    id: "",
    name: "",
    email: "",
    role: "",
    rawPassword: ""
  };

  componentDidMount() {
    this.reloadUsers();
  }

  async reloadUsers() {
    const users = await UserApi.list();
    const info = await UserApi.info();
    this.setState({
      users,
      info
    });
  }

  setValue(name, value) {
    this.setState({
      [name]: value
    });
  }

  get isReady() {
    const { users, info } = this.state;
    return users && info;
  }

  get isEditing() {
    return this.state.selectedRecordId && true;
  }

  get canSubmit() {
    const { name, email, role, rawPassword, selectedRecordId } = this.state;
    if (!selectedRecordId && rawPassword.length === 0) {
      return false;
    }
    return name.length !== 0 && email.length !== 0 && role.length !== 0;
  }

  async insert() {
    const { id, name, email, role, rawPassword } = this.state;
    const payload = {
      id,
      name,
      email,
      role,
      rawPassword
    };
    await UserApi.insert(payload);
    this.reset();
    await this.reloadUsers();
  }

  async update() {
    const { name, email, role, rawPassword, selectedRecordId } = this.state;
    const payload = {
      name,
      email,
      role,
      rawPassword
    };
    await UserApi.update(selectedRecordId, payload);
    this.reset();
    await this.reloadUsers();
  }

  async delete(record) {
    await UserApi.delete(record.id);
    this.reset();
    await this.reloadUsers();
  }

  edit(record) {
    this.setState({
      name: record.name || "",
      email: record.email || "",
      role: record.role || "",
      selectedRecordId: record.id
    });
  }

  reset() {
    this.setState({
      rawPassword: "",
      name: "",
      email: "",
      role: "",
      selectedRecordId: null
    });
  }

  render() {
    if (!this.isReady) return <div>Loading</div>;
    return (
      <div className="user">
        {this.renderForm()}
        {this.renderTable()}
      </div>
    );
  }

  renderForm() {
    const { modeTitle, id, name, email, role, rawPassword } = this.state;
    return (
      <div className="user__section">
        <div className="user__section-header">{modeTitle}</div>
        <div className="user__section-content">
          <table>
            <tr>
              <th>id</th>
              <td>
                <input
                  type="text"
                  value={id}
                  onChange={event => this.setValue("id", event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>Name</th>
              <td>
                <input
                  type="text"
                  value={name}
                  onChange={event => this.setValue("name", event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>Email</th>
              <td>
                <input
                  type="text"
                  value={email}
                  onChange={event => this.setValue("email", event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>Role</th>
              <td>
                <input
                  type="text"
                  value={role}
                  onChange={event => this.setValue("role", event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>Password</th>
              <td>
                <input
                  type="password"
                  value={rawPassword}
                  onChange={event =>
                    this.setValue("rawPassword", event.target.value)
                  }
                />
              </td>
            </tr>
          </table>
          {this.renderFormButtons()}
        </div>
      </div>
    );
  }

  renderFormButtons() {
    if (!this.isEditing)
      return (
        <>
          <button disabled={!this.canSubmit} onClick={() => this.insert()}>
            Submit
          </button>
          <button onClick={() => this.reset()}>Reset</button>
        </>
      );
    return (
      <>
        <button disabled={!this.canSubmit} onClick={() => this.update()}>
          Save
        </button>
        <button onClick={() => this.reset()}>Cancel</button>
      </>
    );
  }

  renderTable() {
    const recordElements = [];
    const { users, info } = this.state;
    for (const record of users) {
      recordElements.push(this.renderTableRecord(record));
    }
    return (
      <div className="user__section">
        <div className="user__section-header">Records ({info.count})</div>
        <div className="user__section-content">
          <table className="user__table">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>{recordElements}</tbody>
          </table>
        </div>
      </div>
    );
  }

  renderTableRecord(record) {
    const { name, email, role, id } = record;
    return (
      <tr>
        <td>{id}</td>
        <td>{name}</td>
        <td>{email}</td>
        <td>{role}</td>
        <td>
          <button onClick={() => this.edit(record)}>Edit</button>
          <button onClick={() => this.delete(record)}>Delete</button>
        </td>
      </tr>
    );
  }
}
