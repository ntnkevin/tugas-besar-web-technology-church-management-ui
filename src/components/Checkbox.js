import React, { Component } from "react";

export default class Checkbox extends Component {
  static defaultProps = {
    styleType: "default",
    clickableLabel: true,
    isChecked: false,
    onClick: () => {}
  };

  render() {
    const {
      tabIndex,
      onClick,
      clickableLabel,
      isChecked,
      children
    } = this.props;
    const additionalProperties = {};
    let additionalClasses = "";
    if (clickableLabel) {
      additionalProperties.onClick = () => onClick(!isChecked);
      additionalClasses = "checkbox--clickable-label";
    }
    return (
      <div
        tabIndex={tabIndex}
        className={`checkbox ${additionalClasses}`}
        {...additionalProperties}
      >
        {this.renderCheck()}
        <span className="checkbox__label">{children}</span>
      </div>
    );
  }

  renderCheck() {
    const { isChecked, onClick, clickableLabel } = this.props;
    const additionalProperties = {};
    if (!clickableLabel) {
      additionalProperties.onClick = () => onClick();
    }
    const checkedStateClass = isChecked
      ? "checkbox__icon--checked"
      : "checkbox__icon--unchecked";
    return (
      <span
        className={`checkbox__icon ${checkedStateClass}`}
        {...additionalProperties}
      />
    );
  }
}
