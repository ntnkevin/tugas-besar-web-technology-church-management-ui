import React, { Component } from "react";
import CongregationApi from "../api/CongregationApi";
export default class UpdateCongregation extends Component {
    state = {
        congregations: null,
        info: null,
        //modeTitle: "New Record",
        id: "",
        fullname: "",
        address: "",
        birthdate: "",
        type: ""
    };

    constructor(props) {
        super(props)
        this.state.id = this.props.match.params.id;
    }

    routeChange(path) {
        this.props.history.push(path);
    }

    async update() {
        const { fullname, address, birthdate, type, id } = this.state;
        const payload = {
            fullname,
            address,
            birthdate,
            type,
        };

        await CongregationApi.update(id, payload);
        this.routeChange('/admin/congregation');
        await this.reloadCongregations();
    }

    componentDidMount() {
        this.reloadUser();
    }

    async reloadUser() {
        const { id } = this.state;
        const user = await CongregationApi.get(id);
        const { fullname, address, birthdate, type } = user;
        this.setState({
            fullname,
            address,
            birthdate,
            type
        });
    }

    reset() {
        this.setState({
            fullname: "",
            address: "",
            birthdate: "",
            type: "",
        });
    }


    setValue(name, value) {
        this.setState({
            [name]: value
        });
    }
    render() {
        const { fullname, address, birthdate, type } = this.state;
        return (
            <div className="full-size flex-center">
                <form class='form-add-congregation' id='form-add'>
                    <center><h3>Update Congregation</h3></center>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <label>
                                    <b>
                                        Full Name:
              </b>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input
                                        name="fullName"
                                        type="text"
                                        size="48"
                                        placeholder="Full Name"
                                        value={fullname}
                                        onChange={event =>
                                            this.setValue("fullname", event.target.value)
                                        } />
                                </label>
                            </td>
                        </tr>

                        <br />

                        <tr>
                            <td>
                                <label>
                                    <b>
                                        Address:
            </b>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input
                                        name="address"
                                        type="text"
                                        size="48"
                                        placeholder="Address"
                                        value={address}
                                        onChange={event =>
                                            this.setValue("address", event.target.value)
                                        } />
                                </label>
                            </td>
                        </tr>

                        <br />

                        <tr>
                            <td>
                                <label>
                                    <b>
                                        Birth Date:
            </b>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input
                                        name="birthDate"
                                        type="text"
                                        size="48"
                                        placeholder="Birth Date"
                                        value={birthdate}
                                        onChange={event =>
                                            this.setValue("birthdate", event.target.value)
                                        } />
                                </label>
                            </td>
                        </tr>

                        <br />

                        <tr>
                            <td>
                                <label>
                                    <b>
                                        Congregation Type:
            </b>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input
                                        name="congregationType"
                                        type="text"
                                        size="48"
                                        placeholder="Congregation Type"
                                        value={type}
                                        onChange={event =>
                                            this.setValue("type", event.target.value)
                                        } />
                                </label>
                            </td>
                        </tr>
                    </table>

                    <br />

                    <label><button class="button-submit-add-congregation" onClick={() => this.update()}><b>Submit</b></button></label>
                    <label><button class="button-reset-add-congregation" onClick={() => this.reset()}><b>Reset</b></button></label>

                </form>
            </div>
        );
    }
}