import React, { Component } from "react";

export default class DropDownContent extends Component {
  static defaultProps = {
    className: ""
  };

  state = {};

  render() {
    const { className } = this.props;
    return (
      <div className={`dropdown__content ${className}`}>
        {this.props.children}
      </div>
    );
  }
}
