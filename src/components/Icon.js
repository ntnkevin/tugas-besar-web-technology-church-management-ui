import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCaretDown,
  faQuestionCircle,
  faBars
} from "@fortawesome/free-solid-svg-icons";

const iconMaps = {
  caretDown: faCaretDown,
  bars: faBars,
  default: faQuestionCircle
};

export default class Icon extends Component {
  static defaultProps = {
    name: null,
    className: "",
    color: "#000"
  };

  state = {};

  render() {
    const { name, className, color } = this.props;
    const icon = iconMaps[name] || iconMaps.default;
    return <FontAwesomeIcon color={color} className={className} icon={icon} />;
  }
}
