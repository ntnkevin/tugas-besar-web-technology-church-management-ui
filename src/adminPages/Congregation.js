import React, { Component } from "react";
import CongregationApi from "../api/CongregationApi";

export default class User extends Component {
  state = {
    users: null,
    info: null,
    selectedRecordId: null,
    modeTitle: "New Record",

    id: "",
    fullname: "",
    address: "",
    type: "",
  };

  componentDidMount() {
    this.reloadCongregations();
  }

  async reloadCongregations() {
    const congregations = await CongregationApi.list();
    const info = await CongregationApi.info();
    this.setState({
      congregations,
      info
    });
  }

  setValue(name, value) {
    this.setState({
      [name]: value
    });
  }

  get isReady() {
    const { congregations, info } = this.state;
    return congregations && info;
  }

  get isEditing() {
    return this.state.selectedRecordId && true;
  }

  get canSubmit() {
    const { fullname, address, birthdate, type, selectedRecordId } = this.state;
    if (!selectedRecordId && type.length === 0) {
      return false;
    }
    return fullname.length !== 0 && address.length !== 0 && birthdate.length !== 0 && type.length !== 0;
  }

  async insert() {
    const { id, fullname, address, birthdate, type } = this.state;
    const payload = {
      id,
      fullname,
      address,
      birthdate,
      type
    };
    await CongregationApi.insert(payload);
    this.reset();
    await this.reloadCongregations();
  }

  async update() {
    const { fullname, address, birthdate, type, selectedRecordId } = this.state;
    const payload = {
      fullname,
      address,
      birthdate,
      type
    };
    await CongregationApi.update(selectedRecordId, payload);
    this.reset();
    await this.reloadCongregations();
  }

  async delete(record) {
    await CongregationApi.delete(record.id);
    this.reset();
    await this.reloadCongregations();
  }

  edit(record) {
    this.setState({
      fullname: record.fullname || "",
      address: record.address || "",
      birthdate: record.birthdate || "",
      type: record.type || "",
      selectedRecordId: record.id
    });
  }

  reset() {
    this.setState({
      fullname: "",
      address: "",
      birthdate: "",
      type: "",
      selectedRecordId: null
    });
  }

  render() {
    if (!this.isReady) return <div>Loading</div>;
    return (
      <div className="user">
        {this.renderForm()}
        {this.renderTable()}
      </div>
    );
  }

  renderForm() {
    const { modeTitle, id, fullname, address, birthdate, type } = this.state;
    return (
      <div className="user__section">
        <div className="user__section-header">{modeTitle}</div>
        <div className="user__section-content">
          <table>
            <tr>
              <th>fullname</th>
              <td>
                <input
                  type="text"
                  value={fullname}
                  onChange={event => this.setValue("fullname", event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>address</th>
              <td>
                <input
                  type="text"
                  value={address}
                  onChange={event => this.setValue("address", event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>birthdate</th>
              <td>
                <input
                  type="text"
                  value={birthdate}
                  onChange={event => this.setValue("birthdate", event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>type</th>
              <td>
                <input
                  type="text"
                  value={type}
                  onChange={event =>
                    this.setValue("type", event.target.value)
                  }
                />
              </td>
            </tr>
          </table>
          {this.renderFormButtons()}
        </div>
      </div>
    );
  }

  renderFormButtons() {
    if (!this.isEditing)
      return (
        <>
          <button disabled={!this.canSubmit} onClick={() => this.insert()}>
            Submit
          </button>
          <button onClick={() => this.reset()}>Reset</button>
        </>
      );
    return (
      <>
        <button disabled={!this.canSubmit} onClick={() => this.update()}>
          Save
        </button>
        <button onClick={() => this.reset()}>Cancel</button>
      </>
    );
  }

  renderTable() {
    const recordElements = [];
    const { congregations, info } = this.state;
    for (const record of congregations) {
      recordElements.push(this.renderTableRecord(record));
    }
    return (
      <div className="user__section">
        <div className="user__section-header">Records ({info.count})</div>
        <div className="user__section-content">
          <table className="user__table">
            <thead>
              <tr>
                <th>Id</th>
                <th>Fullame</th>
                <th>Address</th>
                <th>Birthdate</th>
                <th>Type</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>{recordElements}</tbody>
          </table>
        </div>
      </div>
    );
  }

  renderTableRecord(record) {
    const { fullname, address, birthdate, type, id } = record;
    return (
      <tr>
        <td>{id}</td>
        <td>{fullname}</td>
        <td>{address}</td>
        <td>{birthdate}</td>
        <td>{type}</td>
        <td>
          <button onClick={() => this.edit(record)}>Edit</button>
          <button onClick={() => this.delete(record)}>Delete</button>
        </td>
      </tr>
    );
  }
}
