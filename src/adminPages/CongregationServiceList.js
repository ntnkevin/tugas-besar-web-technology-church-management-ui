import React, { Component } from "react";
import ReactDOM from 'react-dom';
import CongregationServiceApi from "../api/CongregationServiceApi";
import CongregationApi from "../api/CongregationApi";
import ServiceApi from "../api/ServiceApi";
import CongregationServiceRoleApi from "../api/CongregationServiceRoleApi";
// import CongregationTypeApi from "../api/CongregationTypeApi";
// import ServiceTypeApi from "../api/ServiceTypeApi";
import { ComboBox } from '@progress/kendo-react-dropdowns';
import Button from '../components/Button';
export default class CongregationServiceRoleList extends Component {
  state = {
    //congregationservice
    congregationservices: null,
    info: null,
    selectedRecordId: null,
    serviceid: "",
    congregationid: "",
    role: "",

    //congregation
    congregations: null,
    con_info: null,
    selectedCongregationId: null,
    fullname: "",
    address: "",
    birthdate: "",
    congregationtype: "",

    //service
    services: null,
    ser_info: null,
    selectedServiceId: null,
    title: "",
    dateandtime: "",
    servicetype: "",

    //congregationservicerole
    congregationserviceroles: null,
    conservrole_info: null,
    selectedCongregationServiceRoleId: null,
    congregationservice_role: "",

    // //congregationtype
    // congregationtypes: null,
    // contype_info: null,
    // selectedCongregationTypeId: null,
    // congregation_type: "",

    // //servicetype
    // servicetypes: null,
    // sertype_info: null,
    // selectedServiceType: null,
    // service_type: "",

    allowCustom: false,
  };

  routeChange(path) {
    this.props.history.push(path);
  }

  componentDidMount() {
    this.reloadCongregationService();
  }

  async reloadCongregationService() {
    const congregationservices = await CongregationServiceApi.list();
    const congregations = await CongregationApi.list();
    const services = await ServiceApi.list();
    const congregationserviceroles = await CongregationServiceRoleApi.list();
    // const servicetypes = await ServiceTypeApi.list();
    // const congregationtypes = await CongregationTypeApi.list();
    const info = await CongregationServiceApi.info();
    this.setState({
      congregationservices,
      congregations,
      services,
      congregationserviceroles,
      // congregationtypes,
      // servicetypes,
      info
    });
  }

  async update() {
    const {
      congregationservices,
      info,
      selectedRecordId,
      serviceid,
      congregationid,
      role,

      // congregations,
      con_info,
      selectedCongregationId,
      fullname,
      address,
      birthdate,
      congregationtype,

      // services,
      ser_info,
      selectedServiceId,
      title,
      dateandtime,
      servicetype,

      // congregationserviceroles,
      conservrole_info,
      selectedCongregationServiceRoleId,
      congregationservice_role,

      // // congregationtypes,
      // // contype_info,
      // selectedCongregationTypeId,
      // congregation_type,

      // // servicetypes,
      // // sertype_info,
      // selectedServiceTypeId,
      // service_type,
    } = this.state;
    const payloadCongregationService = {
      serviceid,
      congregationid,
      role,
    };
    const payloadCongregation = {
      fullname,
      address,
      birthdate,
      congregationtype,
    };
    const payloadService = {
      title,
      dateandtime,
      servicetype,
    };
    const payloadCongregationServiceRole = {
      congregationservice_role,
    };
    // const payloadCongregationType = {
    //   congregation_type,
    // };
    // const payloadServiceType = {
    //   service_type,
    // };
    await CongregationServiceApi.update(selectedRecordId, payloadCongregationService);
    await CongregationApi.update(selectedCongregationId, payloadCongregation);
    await ServiceApi.update(selectedServiceId, payloadService);
    await CongregationServiceRoleApi.update(selectedCongregationServiceRoleId, payloadCongregationServiceRole);
    // await CongregationTypeApi.update(selectedCongregationTypeId, payloadCongregationType);
    // await ServiceTypeApi.update(selectedServiceTypeId, payloadServiceType);
    this.reset();
    await this.reloadCongregationService();
  }

  async delete(record) {
    await CongregationServiceApi.delete(record.id);
    this.reset();
    await this.reloadCongregationService();
  }

  reset() {
    this.setState({
      //congregationservice
      congregationservices: null,
      info: null,
      selectedRecordId: null,
      serviceid: "",
      congregationid: "",
      role: "",

      //congregation
      congregations: null,
      con_info: null,
      selectedCongregationId: null,
      fullname: "",
      address: "",
      birthdate: "",
      congregationtype: "",

      //service
      services: null,
      ser_info: null,
      selectedServiceId: null,
      title: "",
      dateandtime: "",
      servicetype: "",

      //congregationservicerole
      congregationserviceroles: null,
      conservrole_info: null,
      selectedCongregationServiceRoleId: null,
      congregationservice_role: "",

      // //congregationtype
      // congregationtypes: null,
      // contype_info: null,
      // selectedCongregationTypeId: null,
      // congregation_type: "",

      // //servicetype
      // servicetypes: null,
      // sertype_info: null,
      // selectedServiceType: null,
      // service_type: "",
    });
  }


  setValue(name, value) {
    this.setState({
      [name]: value
    });
  }

  get isReady() {
    const {
      congregationservices,
      congregations,
      services,
      congregationserviceroles,
      // congregationtypes,
      // servicetypes,
      info
    } = this.state;
    return congregationservices && info;
  }

  get isEditing() {
    return this.state.selectedRecordId && true;
  }



  edit(record) {
    this.setState({
      serviceid: record.serviceid || "",
      congregationid: record.congregationid || "",
      role: record.role || "",
      selectedRecordId: record.id,
      
      selectedCongregationId: record.selectedCongregationId,
      fullname: record.fullname || "",
      address: record.address || "",
      birthdate: record.birthdate || "",
      congregationtype: record.congregation_type || "",

      //service
      selectedServiceId: record.selectedServiceId,
      title: record.title || "",
      dateandtime: record.dateandtime || "",
      servicetype: record.service_type || "",

      //congregationservicerole
      selectedCongregationServiceRoleId: record.selectedCongregationServiceRoleId,
      congregationservice_role: record.congregationservice_role || "",

      // //congregationtype
      // selectedCongregationTypeId: record.selectedCongregationTypeId,
      // congregation_type: record.congregation_type || "",

      // //servicetype
      // selectedServiceType: record.selectedServiceTypeId,
      // service_type: record.service_type || "",
    });
    this.routeChange(`/admin/congregationservice/${record.id}`);
  }

  render() {
    if (!this.isReady) return <div>Loading</div>;
    return (
      <div className="user">
        {this.renderTable()}
      </div>
    );
  }

  serviceLists = []
  congregationLists = []
  picked_ids = {
    serv_id: "",
    cong_id: ""
  }

  renderTable() {
    const recordElements = [];
    const { 
      info,
      congregationservices,
      congregations,
      services,
      congregationserviceroles,
      // congregationtypes,
      // servicetypes,
    } = this.state;
    for (const record of congregationservices) {
      recordElements.push(this.renderTableRecord(record));
      this.serviceLists.push(record.Service.id);
      this.congregationLists.push(record.Congregation.id);
    }

    onchange = (event) => {
      this.setState({
        allowCustom: event.target.checked
      });
    }

    return (
      <div className="user__section">
        <div className="user__section-content">
          <div className="user__section-header"><h2><center>Pelayanan Kebaktian Gereja XXX: ({info.count})</center></h2></div>
          <table table id='congregationservices' class='table-congregation-list'>
            <thead>
              <tr class='table-congregation-list__th'>
                <th>service</th>
                <th>congregation</th>
                <th>role</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>{recordElements}</tbody>
            <div>
              {/* <div>Favorite sport:</div> */}
              <ComboBox data={this.serviceLists} allowCustom={false} />
            </div>
          </table>
          <Button onClick={() => this.routeChange('/admin/congregationservice/create')}>Add Congregation Service</Button>

        </div>
      </div>
    );
  }
  renderTableRecord(record) {
    const { 
      id,
    } = record;
    return (
      <tr key={`row-${id}`}>
        <td>{record.Service.title}</td>
        <td>{record.Congregation.fullname}</td>
        <td>{record.CongregationServiceRole.role}</td>
        <td><Button onClick={() => this.edit(record)}>Edit</Button></td>
        <td><Button onClick={() => this.delete(record)}>Delete</Button></td>
      </tr>
    );
  }
}
