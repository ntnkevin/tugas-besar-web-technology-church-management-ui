import React, { Suspense, Component } from "react";
import { Route, Switch } from "react-router-dom";
import { Icon, DropDown, DropDownContent } from "../../components";
import routes from "../../config/AdminRoutes";

const loading = <div>Loading</div>;

export default class Layout extends Component {
  state = {
    isMenuExpanded: false,
    menu: [
      {
        target: "",
        text: "Dashboard"
      },
      {
        target: "congregation/create",
        text: "Add Congregation"
      },
      {
        target: "service/create",
        text: "Add Service"
      },
      {
        target: "congregation",
        text: "Congregation List"
      },
      {
        target: "service",
        text: "Service List"
      },
      {
        target: "congregationservice",
        text: "CongregationServiceList"
      },
      {
        target: "congregationtype",
        text: "Congregation Type"
      },
      {
        target: "servicetype",
        text: "Service Type"
      },
      {
        target: "congregationservicerole",
        text: "CongregationService Role"
      },
    ],
    userDropdownMenu: [
      {
        text: "Profile",
        onClick: () => {}
      },
      {
        text: "Logout",
        onClick: () => {}
      }
    ]
  };

  toggleMenu() {
    this.setState({
      isMenuExpanded: !this.state.isMenuExpanded
    });
  }

  render() {
    console.log(this.props);
    const routes = this.renderRoutes();
    const { isMenuExpanded } = this.state;
    const classNames = isMenuExpanded ? "admin-layout--menu-expanded" : "";
    return (
      <div className={`admin-layout ${classNames}`}>
        <div className="admin-layout__middle">
          {this.renderHeader()}
          <Suspense fallback={loading}>
            <div className="admin-layout__content">
              <Switch>{routes}</Switch>
            </div>
          </Suspense>
        </div>
        {this.renderMenu()}
      </div>
    );
  }

  renderHeader() {
    return (
      <div className="admin-layout__header">
        <DropDown className="admin-layout__user-menu-pill">
          <div className="admin-layout__user-menu-pill-name">Administrator</div>
          <div className="admin-layout__user-menu-pill-avatar"></div>
          <DropDownContent alignment="bottom-right">
            <div className="admin-layout__user-menu-dropdown">
              {this.renderUserMenuOptions()}
            </div>
          </DropDownContent>
        </DropDown>
      </div>
    );
  }

  renderUserMenuOptions() {
    const menuElements = [];
    for (const menu of this.state.userDropdownMenu) {
      menuElements.push(this.renderUserMenuOption(menu));
    }
    return menuElements;
  }

  renderUserMenuOption(menu) {
    return (
      <div
        onClick={() => menu.action()}
        className="admin-layout__user-menu-dropdown-item"
        key={`menu-${menu.text}`}
      >
        {menu.text}
      </div>
    );
  }

  renderMenu() {
    const menuElements = [];
    const { menu } = this.state;
    for (const m of menu) {
      menuElements.push(this.renderMenuElement(m));
    }
    return (
      <div className="admin-layout__menu">
        <div
          className="admin-layout__menu-toggle-button"
          onClick={() => this.toggleMenu()}
        >
          <Icon name="bars" color="white" />
        </div>
        <div className="admin-layout__menu-container">
          <div className="admin-layout__menu-header"></div>
          {menuElements}
        </div>
      </div>
    );
  }

  renderMenuElement(menu) {
    const { navigate } = this.props;
    return (
      <div
        onClick={() => navigate("/admin/" + menu.target)}
        className="admin-layout__menu-row"
        key={`menu-${menu.text}`}
      >
        {menu.text}
      </div>
    );
  }

  renderRoutes() {
    const routeElements = [];
    for (let i = 0; i < routes.length; i += 1) {
      const key = `route-${i}`;
      const route = routes[i];
      const { path, name } = route;
      routeElements.push(
        <Route
          key={key}
          path={path}
          name={name}
          exact={true}
          render={props => <route.component {...props} />}
        />
      );
    }
    return routeElements;
  }
}
