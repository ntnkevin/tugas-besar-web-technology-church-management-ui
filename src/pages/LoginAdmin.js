import React from "react";
import BasePage from './BasePage';
import SessionApi from '../api/SessionApi';
import UserTokenStorage from '../storage/UserTokenStorage';

export default class Home extends BasePage {
  state = {
    email: '',
    password: '',
    me: '',
  };

  componentDidMount(){
    this.refreshUserInfo();
  }

  async refreshUserInfo(){
    const userToken = UserTokenStorage.get();
    if (userToken){

    const me = await SessionApi.myInfo();
    this.setState({
      me: JSON.stringify(me),
    })
    }
  }

  async login(){
    const {
      email,
      password,
    } = this.state;
    const result = await SessionApi.login(email,password);
    UserTokenStorage.set(result.token);
    await this.refreshUserInfo();
  }

  logout(){
    UserTokenStorage.clear();
    this.refreshUserInfo();
    this.setState({
      me: '',
    })
  }

  render() {
    const {
      email,
      password,
      me,
    } = this.state;
    return (
      <div className="full-size flex-center">
        <div>
        Email: <input type="text" value={email} onChange={(event)=>this.setValue('email',event)}/><br/>
        Password: <input type="text" value={password} onChange={(event)=>this.setValue('password',event)}/><br/>
        <button onClick={()=>this.login()}>Login</button>
        <button onClick={()=>this.logout()}>Logout</button>
        <div>{me}</div>
        </div>
      </div>
    );
  }
}
