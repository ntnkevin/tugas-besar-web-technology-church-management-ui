import { lazy } from "react";

const Home = lazy(() => import("../pages/Home"));
const User = lazy(() => import("../adminPages/User"));
const Congregation = lazy(() => import("../adminPages/Congregation"));
const CongregationService = lazy(() => import("../adminPages/CongregationService"));
const Service = lazy(() => import("../adminPages/Service"));

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/user",
    name: "User",
    component: User
  },
  {
    path: "/congregation",
    name: "Congregation",
    component: Congregation
  },
  {
    path: "/congregationservice",
    name: "CongregationService",
    component: CongregationService
  },
  {
    path: "/service",
    name: "Service",
    component: Service
  }
];

export default routes;
