import { lazy } from "react";

const Dashboard = lazy(() => import("../adminPages/Dashboard"));
const AddService = lazy (() => import("../adminPages/AddService"));
const AddCongregationService = lazy(() => import("../adminPages/AddCongregationService"));
const AddCongregation = lazy(() => import("../adminPages/AddCongregation"));
const CongregationList = lazy (() => import("../adminPages/CongregationList"));
const UpdateCongregation = lazy (() => import("../adminPages/UpdateCongregation"));
const CongregationTypeList = lazy (() => import("../adminPages/CongregationTypeList"));
const CongregationServiceList = lazy (() => import("../adminPages/CongregationServiceList"));
const CongregationServiceRoleList = lazy (() => import("../adminPages/CongregationServiceRoleList"));
const ServiceList = lazy (() => import("../adminPages/ServiceList"));
const ServiceTypeList = lazy (() => import("../adminPages/ServiceTypeList"));
const UpdateService = lazy (() => import("../adminPages/UpdateService"));
const UpdateServiceType = lazy (() => import("../adminPages/UpdateServicetype"));
const UpdateCongregationType = lazy (() => import("../adminPages/UpdateCongregationtype"));
const UpdateCongregationServiceRole = lazy (() => import("../adminPages/UpdateCongregationServicerole"));
const routes = [
  {
    path: "",
    name: "Dashboard",
    component: Dashboard
  },
  {
    path: "congregation/create",
    name: "AddCongregation",
    component: AddCongregation
  },
  {
    path: "congregation",
    name: "CongregationList",
    component:CongregationList
  },
  {
    path: "congregationtype",
    name: "CongregationTypeList",
    component:CongregationTypeList
  },
  {
    path: "congregationtype/:id",
    name: "UpdateCongregationType",
    component: UpdateCongregationType
  },
  {
    path: "congregationservicerole",
    name: "CongregationServiceRoleList",
    component:CongregationServiceRoleList
  },
  {
    path: "congregationservicerole/:id",
    name: "UpdateCongregationServiceRole",
    component: UpdateCongregationServiceRole
  },
  {
    path: "service/create",
    name: "AddService",
    component: AddService
  },
  {
    path: "congregation/:id",
    name: "UpdateCongregation",
    component: UpdateCongregation
  },
  {
    path: "service",
    name: "ServiceList",
    component:ServiceList
  },
  {
    path: "service/:id",
    name: "UpdateService",
    component: UpdateService
  },
  {
    path: "congregationservice",
    name: "CongregationServiceList",
    component: CongregationServiceList
  },
  {
    path: "congregationservice/create",
    name: "CongregationServiceList",
    component: AddCongregationService
  },
  {
    path: "servicetype",
    name: "ServiceTypeList",
    component:ServiceTypeList
  },
  {
    path: "servicetype/:id",
    name: "UpdateServiceType",
    component: UpdateServiceType
  },
];

routes.forEach(route => {
  route.path = `/admin/${route.path}`;
});

export default routes;
