import ApiClient from '../modules/ApiClient';

const client = new ApiClient({
    baseUrl: '/',
})

export default{
    get,
    getInfo,
    send,
}

async function get(){
    return client.get();
}

async function getInfo(){
    return client.get('info');
}

async function send(data){
    return client.post('', data);
}