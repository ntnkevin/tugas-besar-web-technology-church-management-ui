import React, { Component } from "react";
import Checkbox from "./Checkbox";

export default class MultiCheckbox extends Component {
  static defaultProps = {
    options: [], // {text: string}
    selectedOptions: [],
    onChange: () => {}
  };

  state = {};

  isOptionSelected(option) {
    const { selectedOptions = [] } = this.props;
    return selectedOptions.indexOf(option) !== -1;
  }

  onToggle(option) {
    const { selectedOptions = [], onChange } = this.props;
    const wasSelected = this.isOptionSelected(option);
    const newSelectedOptions = [];
    for (const selectedOption of selectedOptions) {
      if (selectedOption !== option) {
        newSelectedOptions.push(selectedOption);
      }
    }
    if (!wasSelected) {
      newSelectedOptions.push(option);
    }
    onChange(newSelectedOptions);
  }

  render() {
    return <div className="multi-checkbox">{this.renderOptions()}</div>;
  }

  renderOptions() {
    const { options } = this.props;
    const optionElements = [];
    for (const option of options) {
      optionElements.push(this.renderOption(option));
    }
    return optionElements;
  }

  renderOption(option) {
    const { text } = option;
    const isChecked = this.isOptionSelected(option);
    return (
      <div className="multi-checkbox__option" key={`multi-checkbox-${text}`}>
        <Checkbox isChecked={isChecked} onClick={() => this.onToggle(option)}>
          {text}
        </Checkbox>
      </div>
    );
  }
}
