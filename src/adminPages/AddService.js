import React, { Component } from "react";
import ServiceApi from "../api/ServiceApi";
import DatePicker from '../components/DatePicker';
export default class Dashboard extends Component {
    state = {
        services: null,
        info: null,
        selectedRecordId: null,
        //modeTitle: "New Record",
        title: "",
        type: "",
        dateandtime: new Date(),
    };

    routeChange(path) {
        this.props.history.push(path);
    }

    reset() {
        this.setState({
            title: "",
            dateandtime: "",
            type: "",
            selectedRecordId: null
        });
    }

    get canSubmit() {
        const { title, dateandtime, type, selectedRecordId } = this.state;
        if (!selectedRecordId === 0) {
            return false;
        }
        return title.length !== 0 && dateandtime.length !== 0 && type.length !== 0;
    }
    async insert() {
        const { title, dateandtime, type } = this.state;
        const payload = {
            title,
            dateandtime,
            type
        };

        await ServiceApi.insert(payload);
        this.routeChange('/admin/service-list');
        this.reset();
        await this.reloadServices();
    }

    setValue(name, value) {
        this.setState({
            [name]: value
        });
    }
    render() {
        const { title, type } = this.state;
        return (
            <div className="full-size flex-center">
                <form class='form-add-congregation' id='form-add'>
                    <center><h3>Add Service</h3></center><br />
                    <table><tr>
                        <td>
                            <label><b>Title:</b></label>
                        </td>
                    </tr>
                        <tr>
                            <td><label>
                                <input
                                    name="title"
                                    type="text"
                                    size="48"
                                    placeholder="Title of Service"
                                    value={title}
                                    onChange={event =>
                                        this.setValue("title", event.target.value)
                                    } />
                            </label>
                            </td>
                        </tr>
                        <br />
                        <tr><td><label><b>Date And Time</b>
                        </label>
                        </td>
                        </tr>
                        <tr><td>
                            <label>
                                <DatePicker
                                    selected={this.state.dateandtime}
                                    onSelect={this.handleSelect} //when day is clicked
                                    onChange={this.handleChange} //only when value has changed
                                />
                            </label>
                        </td>
                        </tr>
                        <br />
                        <tr>
                            <td><label><b>Service Type:</b></label></td>
                        </tr>
                        <tr>
                            <td>
                            <select 
                                id="service-type"
                                placeholder="Service Type"
                                type="text"
                                // value={type}
                                onChange={event =>
                                this.setValue("type", event.target.value)
                                }
                                >
                                <option value="Singer">Singer</option>
                                <option value="Pemusik">Pemusik</option>
                                <option value="Worship Leader">Worship Leader</option>
                                <option value="Multimedia">Multimedia</option>
                                <option value="Persembahan">Persembahan</option>
                                </select>
                                {/* <input
                                    name="type"
                                    type="text"
                                    size="48"
                                    placeholder="Service Type"
                                    value={type}
                                    onChange={event =>
                                        this.setValue("type", event.target.value)
                                    } /> */}
                            </td>
                        </tr>
                    </table>

                    <br />

                    <label><button class="button-submit-add-congregation" disabled={!this.canSubmit} onClick={() => this.insert()}><b>Submit</b></button></label>
                    <label><button class="button-reset-add-congregation" onClick={() => this.reset()}><b>Reset</b></button></label>

                </form>
            </div>
        );
    }
}