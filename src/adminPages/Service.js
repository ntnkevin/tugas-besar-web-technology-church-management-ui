import React, { Component } from "react";
import ServiceApi from "../api/ServiceApi";

export default class User extends Component {
  state = {
    users: null,
    info: null,
    selectedRecordId: null,
    modeTitle: "New Record",

    id: "",
    title: "",
    type: "",
    dateandtime: "",
  };

  componentDidMount() {
    this.reloadServices();
  }

  async reloadServices() {
    const services = await ServiceApi.list();
    const info = await ServiceApi.info();
    this.setState({
      services,
      info
    });
  }

  setValue(name, value) {
    this.setState({
      [name]: value
    });
  }

  get isReady() {
    const { services, info } = this.state;
    return services && info;
  }

  get isEditing() {
    return this.state.selectedRecordId && true;
  }

  get canSubmit() {
    const { title, type, dateandtime, selectedRecordId } = this.state;
    if (!selectedRecordId && dateandtime.length === 0) {
      return false;
    }
    return title.length !== 0 && type.length !== 0 && dateandtime.length !== 0;
  }

  async insert() {
    const { id, title, type, dateandtime } = this.state;
    const payload = {
      id,
      title,
      type,
      dateandtime
    };
    await ServiceApi.insert(payload);
    this.reset();
    await this.reloadServices();
  }

  async update() {
    const { title, type, dateandtime, selectedRecordId } = this.state;
    const payload = {
      title,
      type,
      dateandtime
    };
    await ServiceApi.update(selectedRecordId, payload);
    this.reset();
    await this.reloadServices();
  }

  async delete(record) {
    await ServiceApi.delete(record.id);
    this.reset();
    await this.reloadServices();
  }

  edit(record) {
    this.setState({
      title: record.title || "",
      type: record.type || "",
      dateandtime: record.dateandtime || "",
      selectedRecordId: record.id
    });
  }

  reset() {
    this.setState({
      title: "",
      type: "",
      dateandtime: "",
      selectedRecordId: null
    });
  }

  render() {
    if (!this.isReady) return <div>Loading</div>;
    return (
      <div className="user">
        {this.renderForm()}
        {this.renderTable()}
      </div>
    );
  }

  renderForm() {
    const { modeTitle, id, title, type, dateandtime } = this.state;
    return (
      <div className="user__section">
        <div className="user__section-header">{modeTitle}</div>
        <div className="user__section-content">
          <table>
            <tr>
              <th>title`</th>
              <td>
                <input
                  type="text"
                  value={title}
                  onChange={event => this.setValue("title", event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>type</th>
              <td>
                <input
                  type="text"
                  value={type}
                  onChange={event => this.setValue("type", event.target.value)}
                />
              </td>
            </tr>
            <tr>
              <th>dateandtime</th>
              <td>
                <input
                  type="text"
                  value={dateandtime}
                  onChange={event => this.setValue("dateandtime", event.target.value)}
                />
              </td>
            </tr>
          </table>
          {this.renderFormButtons()}
        </div>
      </div>
    );
  }

  renderFormButtons() {
    if (!this.isEditing)
      return (
        <>
          <button disabled={!this.canSubmit} onClick={() => this.insert()}>
            Submit
          </button>
          <button onClick={() => this.reset()}>Reset</button>
        </>
      );
    return (
      <>
        <button disabled={!this.canSubmit} onClick={() => this.update()}>
          Save
        </button>
        <button onClick={() => this.reset()}>Cancel</button>
      </>
    );
  }

  renderTable() {
    const recordElements = [];
    const { services, info } = this.state;
    for (const record of services) {
      recordElements.push(this.renderTableRecord(record));
    }
    return (
      <div className="user__section">
        <div className="user__section-header">Records ({info.count})</div>
        <div className="user__section-content">
          <table className="user__table">
            <thead>
              <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Type</th>
                <th>Dateandtime</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>{recordElements}</tbody>
          </table>
        </div>
      </div>
    );
  }

  renderTableRecord(record) {
    const { title, type, dateandtime, id } = record;
    return (
      <tr>
        <td>{id}</td>
        <td>{title}</td>
        <td>{type}</td>
        <td>{dateandtime}</td>
        <td>
          <button onClick={() => this.edit(record)}>Edit</button>
          <button onClick={() => this.delete(record)}>Delete</button>
        </td>
      </tr>
    );
  }
}
