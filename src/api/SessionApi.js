import ApiClient from '../modules/ApiClient';

const client = new ApiClient();

async function login(email,password){
  return client.post('login',{
    email,password,
  })
}

async function myInfo(){
  return client.get('me');
}

export default {
  login,
  myInfo,
}
